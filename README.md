# Assignment 3

### A small react application to translate text to sign language :)

The application har a login page, a translation page and a profile page.
At the login page the user is asked to enter their name.
This is for 'logging in' so that the users search history can be stored.

At the translation page the user can enter words or sentences and have these translated to the spelling of the word using sign language.
The users last 10 translations are stored.

At the profile page the user can view its last 10 translations.
Using the LogOut button the user can delete its translation history.

-------------
The application is not hosted on Heroku as we were told.

I did not manage to get all features working. 
The features missing is use of redux to store data and manage user sessions
and use of redux to communicate with the database.
I use redux to fetch data from the database, but did not manage to get the POST working in time.

All other features should be on place. The user can 'login' from the start page, but the username will only be stored in local storage.
The translation works flawlessly, and the profile page stores all recent searches (for all users) until the LogOut button is pressed.

##Dependencies
- react router dom
- react redux

The dependencies can be installed using following commands:

```bash
npm install react-router-dom
npm install react-redux
```

or by running 
```bash
npm install
```
inside the app directory.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
