import './App.css';
import Start from "./start/Start";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Translate from "./translate/Translate";
import Profile from "./profile/Profile";

function App() {
    return (
        <div className="App">
            {/*<Navigator />*/}
            <BrowserRouter>
                <Switch>
                    <Route path='/' exact>
                        <Redirect to={'/start'}/>
                    </Route>
                    <Route path={'/start'} component={Start}>
                        <Start/>
                    </Route>
                    <Route path='/translate' component={Translate}>
                        <Translate/>
                    </Route>
                    <Route path='/profile' component={Profile}>
                        <Profile/>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
