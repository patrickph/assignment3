import { Nav } from "react-bootstrap";
import {HashRouter, useHistory} from "react-router-dom";
import './Navigator.css'

function Navigator(username = '') {
    // console.log(username)

    let history = useHistory()

    // Clear local storage (last searches) when logging out
    const logOut = () => {
        localStorage.setItem('localHistory', '')
        history.push('/')
    }
    return (
        <nav className={'Navbar'}>
            <HashRouter>
                <Nav.Link href="/" className={'NavbarName'}>
                    TextToSign
                </Nav.Link>
                <Nav.Link href="/profile" className={'NavbarProfile'}>
                    {username.username}
                </Nav.Link>
                <button className={'LogOut'} onClick={logOut}>
                    Log Out
                </button>
            </HashRouter>
        </nav>
    )
}

export default Navigator;