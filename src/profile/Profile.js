import './Profile.css'
import '../App.css'
import Navigator from "../components/Navigator";

function Profile() {

    /* To use with redux and db

    // Need to fetch current user w/redux
    let username = 'USERNAME_HERE'
    let translations = []

    const {db} = useSelector(state => state.db)
    const dispatch = useDispatch()

    // Fetch database
    useEffect(() => {
        dispatch(dbActionFetch())
    }, [])

     */


    // Get local stored history
    let previousTranslations = localStorage.getItem('localHistory')

    if (previousTranslations) {
        previousTranslations = JSON.parse(previousTranslations)
        //Create list object of each search
        previousTranslations = previousTranslations.map(createList, previousTranslations)
    } else {
        previousTranslations = []
    }

    let username = localStorage.getItem('username')
    if (username) {
        username = JSON.parse(username)
    } else {
        username = ''
    }

    function createList(word, index) {
        return <li className={'searchHistory'} key={index}>{word}</li>
    }

    return (
        <>
            <Navigator username={username}/>
            <div className={'content'}>
                <div className={'upperDivProfile'}>
                    <h1 className={'username'}>{username}</h1>
                </div>
                <div className={'lowerDivProfile'}>
                </div>
            </div>
            <div className={'translationHistory'}>
                <h2>
                    Translation history:
                </h2>
                <ul>
                    { previousTranslations }
                </ul>
            </div>
            <img src={'/images/Logo.png'} alt={''} className={'profileLogo'}/>
        </>
    )
}

export default Profile