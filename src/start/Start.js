import './Start.css'
import {useHistory} from "react-router-dom";
import {useEffect, useState} from "react";
import {Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {dbActionFetch} from "../store/state/dbActions";
import Navigator from "../components/Navigator";

function Start() {

    // Initialize input field
    const [inputText, setInputText] = useState('')

    const {db} = useSelector(state => state.db)
    const dispatch = useDispatch()

    // Fetch db
    useEffect(() => {
        dispatch(dbActionFetch())
    }, [])

    let history = useHistory()

    // Press enter to submit
    useEffect(() => {
        const listener = event => {
            if (event.code === "Enter") {
                event.preventDefault()
                submitName()
            }
        }
        document.addEventListener("keydown", listener);
        return () => {
            document.removeEventListener("keydown", listener);
        };
    })

    // Submit your name
    function submitName() {
        if (inputText === '') return;

        localStorage.setItem('username', JSON.stringify(inputText))

        for (let user of db) {
            if (inputText === user.name) {
                history.push('/translate')
                return
            }
        }

        // Spams db without content :(
        // dispatch(dbActionUpdate(inputText))

        history.push('/translate')
    }

    return (
        <>
            <Navigator/>
            <div className='content'>
                <div className='upperDivStart'>
                    <div className={'welcomeMessage'}>
                        <img src={'images/Logo-Hello.png'} className={'startLogo'} alt={'images/Logo.png'}/>
                        <p className='title'>
                            TextToSign
                        </p>
                    </div>
                </div>
                <div className={'lowerDivStart'}>

                </div>
            </div>
            <div className={'nameInputBox'}>
                <input type={'text'} value={inputText} onChange={e => setInputText(e.target.value)} className={'nameInput'} placeholder={'What is your name?'}/>
                <Button className={'nameInputButton'} onClick={submitName}>
                    &#10132;
                </Button>
            </div>
        </>
    )
}

export default Start;