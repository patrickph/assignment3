import {applyMiddleware, combineReducers, createStore} from "@reduxjs/toolkit";
import {composeWithDevTools} from "redux-devtools-extension";
import {dbReducer} from "./state/dbReducer";
import {dbMiddleware} from "./state/dbMiddleware";


const appReducers = combineReducers({
    db: dbReducer
})

export default createStore(
    appReducers,
    composeWithDevTools(
        applyMiddleware(dbMiddleware)
    )
)