export const ACTION_DB_FETCH = '[db] FETCH'
export const ACTION_DB_SET = '[db] SET'
export const ACTION_DB_UPDATE = '[db] UPDATE'

export const dbActionFetch = () => ({
    type: ACTION_DB_FETCH
})

export const dbActionSet = payload => ({
    type: ACTION_DB_SET,
    payload
})

export const dbActionUpdate = name => ({
    type: ACTION_DB_UPDATE,
    name
})