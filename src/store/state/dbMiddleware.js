import {ACTION_DB_FETCH, dbActionSet, ACTION_DB_UPDATE} from "./dbActions";


export const dbMiddleware = ({dispatch}) => next => action => {

    next(action)

    if (action.type === ACTION_DB_FETCH) {
        fetch('http://localhost:8000/users')
            .then(r => r.json())
            .then(users => dispatch(dbActionSet(users)))
            .catch(e => console.log(e.message))

    }
    else if (action.type === ACTION_DB_UPDATE) {
        const newUser = {
            name: action.name
        }
        const data = {
            method: 'POST',
            body: JSON.stringify({newUser})
        }
        return fetch('http://localhost:8000/users', data)
            .then(r => r.json())
    }
}