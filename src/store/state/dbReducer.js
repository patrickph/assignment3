import {ACTION_DB_FETCH, ACTION_DB_SET, ACTION_DB_UPDATE} from "./dbActions";

const initialState = {
    id: '',
    name: '',
    translations: []
}

export function dbReducer(state = initialState, action) {
    switch(action.type) {
        case ACTION_DB_FETCH:
            return {
                ...state
            }

        case ACTION_DB_SET:
            return {
                ...state,
                db: action.payload
            }

        case ACTION_DB_UPDATE:
            return {
                ...state
            }

        default: return state
    }
}