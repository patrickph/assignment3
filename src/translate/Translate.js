import './Translate.css'
import {useEffect, useState} from "react";
import {Button} from "react-bootstrap";
import translateLetter from './TranslateLetter'
//import {useDispatch, useSelector} from "react-redux";
import Navigator from "../components/Navigator";

function Translate() {

    // Initialize text fields
    const [transText, setTransText] = useState([])
    const [inputText, setInputText] = useState('')

    // To use with redux and db
    // const {db} = useSelector(state => state.db)
    // const dispatch = useDispatch()

    let username = localStorage.getItem('username')
    if (username) {
        username = JSON.parse(username)
    } else {
        username = ''
    }

    // Store last searches in local storage
    let localHistory = localStorage.getItem('localHistory')
    if (!localHistory) {
        localHistory = []
    } else {
        localHistory = JSON.parse(localHistory)
    }

    // Press enter to submit
    useEffect(() => {
        const listener = event => {
            if (event.code === "Enter") {
                event.preventDefault()
                translateText(event)
            }
        }
        document.addEventListener("keydown", listener);
        return () => {
            document.removeEventListener("keydown", listener);
        };
    })

    // Translate text from box to sign
    function translateText(event) {
        // Prevent reload
        event.preventDefault()

        // Empty translation field
        setTransText([])

        let translation = []

        // Push translations of all letters to array
        for (let i = 0; i < inputText.length; i++) {
            translation.push(translateLetter(inputText[i], i))
        }

        // To update db with search history
        // dispatch(dbActionUpdate(inputText))

        //Set last searches in local history
        localHistory.push(inputText)
        if (localHistory.length >= 10) {
            localHistory.splice(0, 1)
        }
        localStorage.setItem('localHistory', JSON.stringify(localHistory))

        // Set translated to translated field
        setTransText(translation)
        setInputText('')
    }

    return (
        <>
            <Navigator username={username}/>
            <div className={'content'}>
                <div className={'upperDiv'}>
                    <div className={'translateInputBox'}>
                        <form onSubmit={translateText}>
                            <input type={'text'} value={inputText} onChange={e => setInputText(e.target.value)} className={'translateInput'} placeholder={'Text to translate'}/>
                            <Button className={'translateInputButton'} type={'submit'}>
                                &#10132;
                            </Button>
                        </form>
                    </div>
                </div>
                <div className={'lowerDiv'}>
                    <div className={'translateBox'}>
                        {transText}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Translate