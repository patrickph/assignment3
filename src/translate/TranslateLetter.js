
function translateLetter(letter, index) {

    if (letter === ' ') return <p key={index}></p>
    let sign = `images/individial_signs/${letter.toLowerCase()}.png`;

    // Return image of translated letter
    return <img src={sign} alt={''} height={'50'} width={'50'} key={index} className={'signImage'}/>
}

export default translateLetter